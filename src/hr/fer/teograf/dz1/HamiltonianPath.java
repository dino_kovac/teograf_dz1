package hr.fer.teograf.dz1;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class HamiltonianPath {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.err.println("Need filename for argument! Aborting.");
            System.exit(-1);
        }

        String filename = args[0];

        Graph g = new Graph();
        int weight = 0;

        try (Scanner scanner = new Scanner(new File(filename))) {

            int vertexCount = scanner.nextInt();

            Vertex first;
            Vertex second;
            Edge edge;

            for (int i = 0; i < vertexCount; ++i) {

                for (int j = 0; j < vertexCount; ++j) {

                    int neighbours = scanner.nextInt();

                    if (j < i) {
                        continue;
                    }

                    if (neighbours == 1) {

                        first = new Vertex("v" + Integer.toString(i));
                        second = new Vertex("v" + Integer.toString(j));

                        // prevent duplicate instances of the same vertex
                        if (g.getVertices().contains(first)) {
                            first = g.getVertex(first);
                        }

                        if (g.getVertices().contains(second)) {
                            second = g.getVertex(second);
                        }

                        edge = new Edge(weight, first, second);

                        if (g.getEdges().contains(edge)) {
                            edge = g.getEdge(edge);
                        }

                        g.addEdge(edge);

                    }
                }
            }

            // there is at least one cut-off vertex, which means the graph can't have a hamiltonian path
            if(g.getVertices().size() < vertexCount) {
                System.out.print("0");
                return;
            }

            LinkedHashSet<Vertex> path = findHamiltonianPath(g);
            LinkedHashSet<Vertex> cycle = findHamiltonianCycle(g);

            // prints the hamiltonian path found
//            printPath(path);

            if (!path.isEmpty() && cycle.isEmpty()) {
                System.out.print("1");
            } else {
                System.out.print("0");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static LinkedHashSet<Vertex> findHamiltonianPath(Graph graph) {

        LinkedHashSet<Vertex> path = new LinkedHashSet<>();

        for (Vertex startVertex : graph.getVertices()) {

            if (findPath(startVertex, path, graph)) {
                break;
            }
        }

        return path;
    }

    private static LinkedHashSet<Vertex> findHamiltonianCycle(Graph graph) {

        LinkedHashSet<Vertex> cycle = new LinkedHashSet<>();

        for (Vertex startVertex : graph.getVertices()) {

            if (findCycle(startVertex, cycle, graph)) {
                break;
            }
        }

        return cycle;
    }


    private static boolean findPath(Vertex currentVertex, Set<Vertex> visitedVertices, Graph graph) {

        visitedVertices.add(currentVertex);

        // there is a hamiltonian path
        if (visitedVertices.size() == graph.getVertices().size()) {

            return true;
        }

        for (Vertex neighbour : currentVertex.getNeighbours()) {

            if (!visitedVertices.contains(neighbour)) {

                if (findPath(neighbour, visitedVertices, graph)) {

                    return true;
                }
            }
        }

        visitedVertices.remove(currentVertex);
        return false;
    }

    private static boolean findCycle(Vertex currentVertex, Set<Vertex> visitedVertices, Graph graph) {

        visitedVertices.add(currentVertex);

        // there is a hamiltonian path
        if (visitedVertices.size() == graph.getVertices().size()) {

            // these is a hamiltonian cycle
            if (currentVertex.getNeighbours().contains(visitedVertices.iterator().next())) {
                return true;
            }
        }

        for (Vertex neighbour : currentVertex.getNeighbours()) {

            if (!visitedVertices.contains(neighbour)) {

                if (findCycle(neighbour, visitedVertices, graph)) {

                    return true;
                }
            }
        }

        visitedVertices.remove(currentVertex);
        return false;
    }


    private static void printPath(LinkedHashSet<Vertex> path) {

        System.out.println(path.toString());
    }

}
